TABLE MOVEMENT CHECKER
----------------------
## About
This project checks if a sequence of movements will result in an object falling of a table or not.
If it is still on the table the position of the object will be returned.

The table can be seen as a matrix where the object will have an x and y position
The object always occupies exactly one cell and can be seen as a point without mass. Origo
is at the top left.

![table example](img/table_example.png "Figure 1: Red box represents object at position [1, 2] on a 4 x 4 table.")

## Run
### Start
``python3 main.py``

### Input
Please provide input as comma separated values, e.g. ``1,2,1,2``.
Only positive number values will be accepted all other values will be ignored.

### Initiating the Table and Object
To begin the user must first provide a table size and object starting position.
Please provide four comma separated values, where the first two define the table dimensions and the second set defines where the object will will start.
The object always starts facing north. 

If the program gets 4,4,2,2 as input, the table is initiated to size 4 x 4 with the object in
position [2, 2] with direction north.

### Moving the Object
The object always has a direction (north, east, south or west).
A simulation always starts with direction north. North means that if the object sits on [2, 4]
and moves forward one step, the object will now stand on [2, 3].

### Movement Commands

| input | Action |
| :--: | :------- |
| 0 | quit simulation and print results to stdout |
| 1 | move forward one step |
| 2 | move backwards one step |
| 3 | rotate clockwise 90 degrees (eg north to east) |
| 4 | rotate counterclockwise 90 degrees (eg west to south) |

Any other input not listed above will be ignored.
The script will continue listening for input until a line containing a 0 is provided.
Values after the first 0 will be ignored.

### Output
If the simulation succeeded: The objects final position as two integers [x, y].
If the simulation failed (the object falls off the table): [-1, -1] will be returned.
