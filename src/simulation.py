from src.table import RectangleTable
from src.moving_object import MovingObject


class RectangleTableSimulation:
    def __init__(self, width: int, height: int, x: int, y: int):
        self.on_table = True
        self.table = RectangleTable(width, height)
        self.mover = MovingObject(x, y)
        self.is_in_bound()

    def is_in_bound(self):
        self.on_table = self.table.is_in_bound(self.mover.x, self.mover.y)

    def do_action(self, action: int):
        if self.on_table:
            check_bound = self.mover.do_actions(action)
            if check_bound:
                self.is_in_bound()

    def end(self):
        if self.on_table:
            return self.mover.do_actions(0)
        else:
            return [-1, -1]
