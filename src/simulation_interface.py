import sys
from src.simulation import RectangleTableSimulation


def listener():
    sim = None
    for line in sys.stdin:
        if sim:
            for val in line.split(","):
                val = validate_input(val)
                if val == 0:
                    sys.stdout.write(str(sim.end()))
                    return
                elif val is not None:
                    sim.do_action(val)
        else:
            if line.count(",") == 3:
                init_val = validate_initial_input(line)
                if init_val:
                    sim = RectangleTableSimulation(*init_val)


def validate_input(val: str) -> (int, None):
    """ Helper function: returns a positive integer or None """
    if isinstance(val, str):
        try:
            val = int(val)
        except ValueError:
            return
        else:
            if val >= 0:
                return val


def validate_initial_input(line: str) -> (list, None):
    """ Helper function: returns a list with four ints or None """
    values = []
    for _v in line.split(","):
        _v = validate_input(_v)
        if _v is not None:
            values.append(_v)
    if len(values) == 4:
        return values
