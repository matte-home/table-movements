class MovingObject:
    def __init__(self, x_start: int, y_start: int):
        self.x = x_start
        self.y = y_start
        self.facing = 0
        self.NORTH = 0
        self.EAST = 1
        self.SOUTH = 2
        self.WEST = 3
        self.ACTIONS = [
            "end",
            "move_forward",
            "move_backward",
            "rotate_clockwise",
            "rotate_counterclockwise"
        ]

    def do_actions(self, action: int):
        try:
            action = self.ACTIONS[action]
        except IndexError:
            return
        else:
            return self.__getattribute__(f"action_{action}")()

    # //// ACTIONS ////
    def action_end(self) -> list:
        """ simulation will quit returing results for stdout output """
        return [self.x, self.y]

    def action_move_forward(self):
        """" move forward one step """
        if self.facing == self.NORTH:
            self.y -= 1
        elif self.facing == self.EAST:
            self.x += 1
        elif self.facing == self.SOUTH:
            self.y += 1
        elif self.facing == self.WEST:
            self.x -= 1
        return True  # check if in bound

    def action_move_backward(self):
        """ move backwards one step """
        if self.facing == self.NORTH:
            self.y += 1
        elif self.facing == self.EAST:
            self.x -= 1
        elif self.facing == self.SOUTH:
            self.y -= 1
        elif self.facing == self.WEST:
            self.x += 1
        return True  # check if in bound

    def action_rotate_clockwise(self):
        """ rotate clockwise 90 degrees (eg north to east) """
        self.facing = (self.facing + 1) % 4
        return False  # do not check if in bound

    def action_rotate_counterclockwise(self):
        """ rotate counterclockwise 90 degrees (eg west to south)}"""
        self.facing = (self.facing - 1) % 4
        return False  # do not check if in bound
