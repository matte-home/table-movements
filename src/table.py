class RectangleTable:
    def __init__(self, width: int, height: int):
        self.width = width
        self.height = height

    def is_in_bound(self, x, y):
        if x < 0 or y < 0 or x > self.width - 1 or y > self.height - 1:
            return False
        else:
            return True
