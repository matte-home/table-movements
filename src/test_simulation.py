import unittest
from src.simulation import RectangleTableSimulation


class TestRectangleTableSimulation(unittest.TestCase):
    def test_setup_basics(self):
        trs = RectangleTableSimulation(3, 4, 1, 2)
        self.assertEqual(3, trs.table.width)
        self.assertEqual(4, trs.table.height)
        self.assertEqual(1, trs.mover.x)
        self.assertEqual(2, trs.mover.y)
        self.assertEqual(0, trs.mover.facing)
        self.assertTrue(trs.on_table)

    def test_setup_pass(self):
        trs = RectangleTableSimulation(2, 2, 1, 1)
        self.assertEqual([1, 1], trs.end())

    def test_setup_fail(self):
        trs = RectangleTableSimulation(2, 2, -1, 1)
        self.assertEqual([-1, -1], trs.end())

    def test_action_rotate(self):
        trs = RectangleTableSimulation(2, 2, 1, 1)
        trs.do_action(3)
        self.assertEqual(1, trs.mover.facing)
        trs.do_action(4)
        trs.do_action(4)
        self.assertEqual(3, trs.mover.facing)
        self.assertEqual(1, trs.mover.x)
        self.assertEqual(1, trs.mover.y)

    def test_action_move(self):
        trs = RectangleTableSimulation(2, 2, 1, 1)
        trs.do_action(1)
        self.assertEqual(0, trs.mover.y)
        trs.do_action(2)
        self.assertEqual(1, trs.mover.y)
        self.assertEqual(1, trs.mover.x)
        self.assertEqual(0, trs.mover.facing)

    def test_failing_simulation_north(self):
        trs = RectangleTableSimulation(2, 2, 1, 1)
        trs.do_action(1)
        trs.do_action(1)
        self.assertEqual([-1, -1], trs.end())

    def test_failing_simulation_east(self):
        trs = RectangleTableSimulation(2, 2, 1, 1)
        trs.do_action(3)
        trs.do_action(1)
        self.assertEqual([-1, -1], trs.end())

    def test_passing_simulation(self):
        trs = RectangleTableSimulation(4, 4, 2, 2)
        moves = (1, 4, 1, 3, 2, 3, 2, 4, 1, 0)
        for move in moves:
            trs.do_action(move)
        self.assertEqual([0, 1], trs.end())

    def test_origo_start(self):
        trs = RectangleTableSimulation(3, 3, 0, 0)
        moves = (3, 1, 1)
        for move in moves:
            trs.do_action(move)
        self.assertEqual([2, 0], trs.end())
